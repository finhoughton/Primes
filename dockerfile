From ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update --fix-missing && apt-get install -y python3.9 \
  python3.9-dev python3-pip

RUN pip install pipenv

COPY Pipfile .
COPY Pipfile.lock .
RUN pipenv sync
EXPOSE 8080
ENV PYTHONUNBUFFERED=1

COPY . .

CMD ["pipenv", "run", "uvicorn", "app:app", "--port", "8080", "--host", "0.0.0.0"]
