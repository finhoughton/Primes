from fastapi import APIRouter, Request
from fastapi.responses import HTMLResponse

from dependencies import templates

rooter = APIRouter(responses={404: {'description': 'Not found'}}, tags=['Main Page'], default_response_class=HTMLResponse, prefix='/calculator')

@rooter.get('/', response_class=HTMLResponse, description='The main website')
async def index(request: Request):
    return templates.TemplateResponse('index.html', {'request':request})
