from .Backend import SearchSystem

Search = SearchSystem()

def appendNotNone(a:list, item:any) -> list:
    if item is not None:
        a.append(item)
    return a

def extendNotNone(a:list, item:any) -> list:
    if item is not None:
        a.extend(item)
    return a


def searchMain(ID:int, Prime:int, work_unit:int) -> list[tuple]:
    searchResult:list[tuple] = []
    if ID is not None:
        appendNotNone(searchResult, Search.searchID(ID))
    
    if Prime is not None:
        appendNotNone(searchResult, Search.searchPrime(Prime))

    if work_unit is not None:
        extendNotNone(searchResult, Search.search_work_unit(work_unit))

    return searchResult