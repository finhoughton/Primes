SELECT *
FROM (
    SELECT *, row_number() over (order by prime_number) as row_number
    From prime_numbers
)
where row_number between 0 and 5