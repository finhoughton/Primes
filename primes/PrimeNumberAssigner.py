import primes.Backend as Backend
from datetime import datetime, timedelta

PrimeNumManager = Backend.prime_numbers()

def get_work_unit():
    ExpiryDate = datetime.today() - timedelta(minutes=5)
    work_units = PrimeNumManager.find_unfinished_work_units(ExpiryDate)
    if work_units == []:
        largest_max_range = PrimeNumManager.find_largest_max_range()
        if largest_max_range is None:
            largest_max_range = 2
        min_range = largest_max_range
        max_range = largest_max_range + 1000000
        WorkID = PrimeNumManager.createRange(min_range, max_range)
        print(f"Work Unit {WorkID} generated")
        return {"Range": [min_range, max_range], "WorkID": WorkID}
    else:
        work_unit = work_units[0]
        print(f"Relocating Work Unit {WorkID}")
        PrimeNumManager.update_work_unit_sent(datetime.today(), work_unit[0])
        return {"Range": [work_unit[1], work_unit[2]],"WorkID": work_unit[0]}
    
def add_prime_number(listOfPrimes:list[int], work_unit:int):
    PrimeNumManager.register_prime_numbers(listOfPrimes, work_unit)

def Random():
    RPrime = Backend.randomPrime()
    return RPrime