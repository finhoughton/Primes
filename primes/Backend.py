import psycopg2
from datetime import datetime
import random
from os import listdir

def get0(a:list):
    return a[0] if a else None

def getTodaysDate():
    TodaysDate = datetime.today()
    return TodaysDate

class CreateDB():
    def __init__(self) -> None:
        self.conn = self.createConnection()
        self.apply_migrations()

    def apply_migrations(self):
        migration_files = listdir("./migrations")
        print(migration_files)
        for file in migration_files:
            with open("./migrations/" + file, "r") as sql:
                        cur = self.conn.cursor()
                        cur.execute(sql.read())
                        self.conn.commit()
            

    # DB Setup
    def createConnection(self):
        conn = psycopg2.connect(database = "pythonapp-db-primes", 
                        user = "pythonapp-user-primes", 
                        application_name="python_app_primes",
                        host= '90.214.68.106',
                        password = "4YjEB59UGu5ykeeh",
                        port = 5432)
        return conn
    
    def closeConnection(self):
        self.conn.close()
    
    def updateDB(self, sql:str, values:tuple):
        cur = self.conn.cursor()
        cur.execute(sql + " RETURNING id", values)
        self.conn.commit()
        rowID = cur.fetchone()[0]
        return rowID
    
    def updateManyDB(self, sql:str, values:str):
        cur = self.conn.cursor()
        cur.execute(sql + " " + values)
        rowID = cur.lastrowid
        self.conn.commit()
        return rowID
        
    def selectDB(self, sql:str):
        cur = self.conn.cursor()
        cur.execute(sql)
        self.conn.commit()
        rows = cur.fetchall()
        return rows

class prime_numbers(CreateDB):
    
    def find_unfinished_work_units(self, ExpiryDate):
        results = self.selectDB("SELECT id, min_range, max_range, date_sent FROM work_units WHERE ID not in (SELECT id FROM prime_numbers)")
        ValidRecords:list = []
        for result in results:
            if result[3] <= ExpiryDate:
                ValidRecords.append(result)
        return ValidRecords
    
    
    def createRange(self, min_range:int, max_range:int): 
        return self.updateDB('INSERT INTO work_units(min_range, max_range, date_sent) VALUES(%s,%s,%s)', (min_range, max_range, getTodaysDate()))
    
    def find_largest_max_range(self):
        return self.selectDB('SELECT max(max_range) FROM work_units')[0][0]

    def randomPrime(self):
        Max = self.selectDB('SELECT max(ID) FROM work_units')[0][0]
        RandomID = random.randint(1, Max)        
        return self.selectDB(f'SELECT * FROM prime_numbers WHERE ID = {RandomID}')[0]
    
    def update_work_unit_sent(self, Date:str, ID:int):
        return self.updateDB(f"UPDATE work_units SET date_sent = '{Date}' WHERE ID = {ID}", [])
    
    def register_prime_numbers(self, listOfPrimes:list, work_unit:int):
        x = ""
        for prime_number in listOfPrimes:
            x += f"({prime_number},{work_unit}),"
        x = x[:-1]
        
        
        result = self.updateManyDB('INSERT INTO prime_numbers(prime_number, work_unit) VALUES', x)
        print(f"Done pushing {work_unit} to db")
        return result
    
class SearchSystem(CreateDB):
    def searchID(self, ID:int):
        return get0(self.selectDB(f'''SELECT prime_numbers.ID, prime_numbers.prime_number, prime_numbers.work_unit, 
SUBSTR(work_units.date_sent, 1, INSTR(work_units.date_sent, ' ')-1)
FROM prime_numbers, work_units 
WHERE prime_numbers.ID = {ID} 
AND prime_numbers.work_unit = work_units.ID'''))
    
    def searchPrime(self, prime_number:int):
        return get0(self.selectDB(f'''SELECT prime_numbers.ID, prime_numbers.prime_number, prime_numbers.work_unit, 
SUBSTR(work_units.date_sent, 1, INSTR(work_units.date_sent, ' ')-1)
FROM prime_numbers, work_units 
WHERE prime_numbers.prime_number = {prime_number} 
AND prime_numbers.work_unit = work_units.ID'''))
        
    def search_work_unit(self, work_unit:int):
        return self.selectDB(f'''SELECT prime_numbers.ID, prime_numbers.prime_number, prime_numbers.work_unit, 
SUBSTR(work_units.date_sent, 1, INSTR(work_units.date_sent, ' ')-1)
FROM prime_numbers, work_units 
WHERE prime_numbers.work_unit = {work_unit} 
AND prime_numbers.work_unit = work_units.ID''')