from requests import request
from . import PrimeNumberAssigner as prime
from fastapi import APIRouter, Form, Request
from fastapi.responses import JSONResponse, HTMLResponse
from typing import Optional

from dependencies import templates
from .search import searchMain

rooter = APIRouter(responses={404: {'description': 'Not found'}}, tags=['Prime Numbers'])

@rooter.get('/GetWorkUnit', response_class=JSONResponse, response_description='Get the range of the prime numbers needed to test')
async def get_work_unit():
    return prime.get_work_unit()


@rooter.post('/ReturnWorkUnit', response_class=JSONResponse)
async def return_work_unit(request: Request):
    response = await request.json()
    prime.add_prime_number(response["PrimeNumbers"], response["WorkUnit"])
    return response["PrimeNumbers"]

@rooter.post('/SearchPrimeResult', response_class=HTMLResponse, response_description='Return the Prime Numbers in a graphical fashion',)
async def SearchPrimeResult(request:Request, prime_numberID:Optional[int] = Form(None), prime_number:Optional[int] = Form(None), work_unit:Optional[int] = Form(None)) -> HTMLResponse:
    searchResult = searchMain(prime_numberID, prime_number, work_unit)
    return templates.TemplateResponse('returnPrimes.html', {'request':request, 'prime_numberList':searchResult})


@rooter.get('/RandomPrime', response_class=HTMLResponse, response_description='Return a random prime number from our database',)
async def RandomPrime(request: Request) -> HTMLResponse:
    return prime.Random()

