function updateClock(){
    document.getElementById('time').innerHTML = document.lastModified
}

msg = document.getElementById('msg')
function updateMSG(){
    fetch('../msg')
	.then(data => data.json())
	.then((json) => {
        console.log(json)
        msg.innerHTML = json['message']
        msg.style.textDecorationColor = json['colour']
    })
}

updateClock()
window.setInterval(updateClock, 1000)
updateMSG()
window.setInterval(updateMSG, 600000)