function calcPrimes(start, end){
    var primeNumbers = []; /* Where the prime numbers are stored */
	for(var dividend = start; dividend<=end; dividend++){
		var root = Math.sqrt(dividend);
		var isPrime = true;
		for(var divisor = 2; divisor < root; divisor++){
			if(dividend % divisor == 0){
				isPrime = false;
				break;
			}
		}
		if (isPrime){
            postMessage([dividend, 'prime'])
		    primeNumbers.push(dividend);
		}
	}
    return primeNumbers
}

async function runPrime(){
	await fetch('../GetWorkUnit')
	.then(data => data.json())
	.then((json) => {
		var start  = json['Range'][0]
		var end    = json['Range'][1]
		var workID = json['WorkID']
		console.log(workID)

		postMessage([workID, 'currentWorkUnit'])
		var primes = calcPrimes(start, end)

		fetch('../ReturnWorkUnit', {
			body: JSON.stringify({'PrimeNumbers': primes, 'WorkUnit': workID}),
			method: 'POST'
		})

	})
	
}


async function main (){
	while (true) {
		await runPrime()
	}
}

main()