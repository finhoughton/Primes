var primesList = document.getElementById('primes')
var workUnit = document.getElementById('workUnit')

const worker = new Worker('../static/minePrime.js')

primesList.start = Math.ceil( primesList.offsetHeight/primesList.firstChild.offsetHeight)

worker.addEventListener("message", event => {
    // console.log(event)
    if (event.data && event.data[1] == 'prime'){
        primesList.insertBefore(document.createElement('li'), primesList.firstChild).innerHTML = event.data[0]

        if (primesList.children.length > Math.floor(primesList.offsetHeight/primesList.firstChild.offsetHeight)) {
            primesList.removeChild(primesList.lastChild)
        }
        primesList.start++
    }
    if (event.data && event.data[1] == 'currentWorkUnit'){
        workUnit.innerHTML = event.data[0]
    }
});