CREATE TABLE IF NOT EXISTS prime_numbers (
        "prime_number"   BIGINT NOT NULL PRIMARY KEY,
        "work_unit"      INTEGER NOT NULL
);