CREATE TABLE IF NOT EXISTS work_units (
        "id"    BIGSERIAL UNIQUE PRIMARY KEY,
        "min_range"      BIGINT NOT NULL UNIQUE,
        "max_range"      BIGINT NOT NULL UNIQUE,
        "date_sent"      timestamp without time zone NOT NULL
);