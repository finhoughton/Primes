from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse, FileResponse, HTMLResponse

from primes import primeAPI
from calculator import MainPage
from dependencies import templates, Data

app = FastAPI()
app.include_router(primeAPI.rooter)
app.include_router(MainPage.rooter)

@app.get('/', response_class=HTMLResponse)
async def home(request:Request):
    return templates.TemplateResponse('home.html', {'request':request})

@app.get('/static/{fileName}', response_class=FileResponse)
async def static(request:Request, fileName:str) -> FileResponse:
    decomposedFileName = fileName.split('.')
    headers={'content-type':'text/plain'}
    if len(decomposedFileName) > 1:
        if decomposedFileName[1] == 'js':
            headers={'content-type':'application/javascript'}
        elif decomposedFileName[1] =='css':
            headers={'content-type':'text/css'}
        elif decomposedFileName[1] =='html':
            headers={'content-type':'text/html'}
    return FileResponse(f'static/{fileName}', headers=headers)

@app.get('/msg', response_class=JSONResponse)
async def getMsg(request: Request):
    return Data.getConfig(Data.SYSTEMVARIABLESDIR)['Message']

@app.get('/download', response_class=HTMLResponse)
async def download(request:Request):
    return templates.TemplateResponse('downloads.html', {'request':request})

@app.get('/Empty', response_class=HTMLResponse)
def Empty() -> HTMLResponse:
    return '<!DOCTYPE html><html style="height:0;"><head><title>Nothing has been returned yet</title></head></html>'