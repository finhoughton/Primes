from distutils.core import setup  # Need this to handle modules
import py2exe
# We have to import all modules used in our program
import os
from requests import get, post
import logging
from multiprocessing import Process, Manager
# Calls setup function to indicate that we're dealing with a single console application
setup(console=['client.py'])
