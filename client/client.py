#  © (Copyright (c)) M-T <https://github.com/Highest-Prime/>
import logging
import os
import math as maths
from multiprocessing import Manager, Process
import time
import random

from requests import get, post


def calcPrimes(start: int, end: int):
    primes = []
    for dividend in range(start, end):
        root = maths.sqrt(dividend)
        for divider in range(2, maths.ceil(root)):
            if (dividend % divider) == 0:
                break
        else:
            primes.append(dividend)
    return primes


def crunch(core):
    while True:
        workUnit = get('https://primes.rubogubo.com/GetWorkUnit').json()

        range = workUnit['Range']
        id = workUnit['WorkID']

        print(
            f' (Core number: {core}) (Work unit: {id}) /GetWorkUnit returned {workUnit}')

        primes = calcPrimes(range[0], range[1])

        returned = {
            'PrimeNumbers': primes,
            'WorkUnit': id
        }

        jsonsend = post(
            'https://primes.rubogubo.com/ReturnWorkUnit', json=returned)
        print(
            f' (Core number: {core}) (Work unit: {id}) /ReturnWorkUnit returned {jsonsend} ({len(returned)} primes)')


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s: %(message)s', level=logging.INFO)
    totalOSCores = os.cpu_count()

    confirm = False
    while not confirm:
        cores = int(input(
            f'How many threads would you like to crunch. Please enter a number between 1 and {totalOSCores} \n: '))

        if not (cores > totalOSCores or cores < 1):
            confirm = True

    manager = Manager()
    processes: list = []
    for core in range(cores):
        p = Process(target=crunch, args=(core,))
        p.start()
        processes.append(p)
        logging.info(f'started Processor: {core}')

    for p in processes:
        p.join()
