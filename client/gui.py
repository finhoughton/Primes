from tkinter import *
from client import run
import os
app = Tk()
app.title("Client")
app.geometry("400x400")
app.iconbitmap("client/phillip.png")
threadcount = os.cpu_count()
cores = Entry(app)
def start():
    threads = cores.get()
    response = run(threads)
    

start = Button(app, text="Start", command=start)
start.pack()

cores.pack()



app.mainloop()